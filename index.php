<!DOCTYPE html">
<html>
	<head>
	<title>PHP domaci broj 1</title>	
	</head>
	<body>
	
<?php
$ime = "Bojan"; //string
$god = 2018; //integer
$flo = 3.50; // float
$auti = array("BMW", "Audi", "Toyota"); //array
$x = true; //boolean
$y = false; //boolean

	{
			echo 'Varijabla tipa <i>string</i>: <span style="color:#ff0000"> '. $ime .' </span>';
			echo '<br>';

			echo 'Varijabla tipa <i>integer</i>: <span style="color:#00c103"> '. $god .' </span>';
			echo '<br>';
			
			echo 'Varijabla tipa <i>float</i>: <span style="color:#001ff5"> '. $flo .' </span>';
			echo '<br>';
			  
			echo 'Varijabla tipa <i>array</i>: <span style="color:#001ff5"> '. $auti[0] . '</span>, <span style="color:#00c103"> ' . $auti[1] .'</span> i <span style="color:#ff0000"> ' . $auti[2] . ' </span>';
			echo '<br>';
			  
			echo 'Varijabla tipa <i>boolean</i>, vrednost tipa TRUE: <span style="color:#ff0000"> ' .$x. '</span>';
			echo '<br>';
			
			echo 'Varijabla tipa <i>boolean</i>, vrednost tipa FALSE: <span style="color:#ff0000"> ' .$y. '</span>';
			echo "<br>";
   } 
	 echo '<br>';
?>

<?php // Izracunavanje sume brojeva
 include 'functions.php';

 echo 'Suma brojeva u nizu je: '. SumColor("red", [6,6,1,7,8,9]) .'';
 echo "<br>\n";

?>

	</body>
</html> 