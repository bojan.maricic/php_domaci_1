<?php 

function SumColor($color, $numbers) {

    // provera da li je $color bas boja
    if($color!="red" && $color!="blue" && $color!="green" && $color!="yellow") {
        $returnValue="Niste uneli boju!";
    }
    // ispunjen uslov
    else {
        $sum = array_sum($numbers);
        $returnValue="<span style=\"color:".$color."\">".$sum."</span>";
    }
    
    return $returnValue;
    
}